# dotfiles
Welcome to my dotfiles for my main system.\
The method I'm using to manage my dotfiles is with GNU Stow.

To use all of my dotfiles:
```
git clone https://gitlab.com/SgtBloody/dotfiles.git "$HOME/dotfiles/"
cd "$HOME/dotfiles/"
stow */
```
or:
```
stow "name of the folder"
```
