# Starship
Cross-shell prompt written in Rust

## Dependencies
* [Starship](https://starship.rs/)
* [NERD-Fonts](https://www.nerdfonts.com/) Enable one of the NERD fonts in your terminal of choice.
