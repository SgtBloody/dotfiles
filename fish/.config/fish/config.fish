#!/usr/bin/fish

set -g fish_greeting			# Suppresses intro message

### Colours
set fish_color_quote "#00e80a"

### Editors
# set -x EDITOR "nano"
# set -x EDITOR "vim"
# set -x EDITOR "nvim"
set -x EDITOR "emacsclient -c"
# set -x EDITOR "emacs"

### Browser
set -x BROWSER brave

### Pagers
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'" 	# Bat
# set -x MANPAGER "nvim -c "set ft=man" -" 			# Neovim


### Set default keybinds or vim-like keybinds
function fish_user_key_bindings
#	fish_default_key_bindings
	fish_vi_key_bindings
end

### Emulates the behaviour of cursor in Vim
set fish_cursor_default block
set fish_cursor_insert line blink
set fish_cursor_replace underscore
set fish_cursor_replace_one underscore

### Environment tables
fish_add_path ~/Scripts
fish_add_path ~/Appimages
fish_add_path ~/.cargo/bin		# Path for Rust env
fish_add_path ~/.emacs.d/bin	# Doom Emacs env
fish_add_path ~/.local/bin

### Locations of dotfiles
set -x STARSHIP_CONFIG ~/.config/starship/starship.toml

### Aliases
alias d="date '+%d/%m/%Y %R'"

# Dotfiles management
alias config="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"
alias dots="config status"
alias dota="config add"
alias dotc="config commit -m"
alias dotp="config push git@gitlab.com:sgtbloody/dotfiles"
alias dotdiff="config diff master | bat"

# Directory/File management
alias ..="cd .."
alias cp="cp -vi"
alias mv="mv -vi"
alias rm="rm -vi"
alias mkd="mkdir -vp"
alias rmd="rmdir -v"
alias ls="exa -l --icons --git"

# Applications
alias nsxiv="nsxiv -fa"
alias sxiv="sxiv -bf"
alias rick="curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash"

# Kittens
alias icat="kitty +kitten icat --align=left"
alias mdcat="mdcat -p"

set -x RANGER_LOAD_DEFAULT_RC false

### Start Starship prompt ###
starship init fish | source
