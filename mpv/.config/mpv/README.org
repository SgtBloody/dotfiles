#+TITLE: mpv
#+author: Bloody Sergeant
Cross-platform light-weight media player

* Scripts I use
+ [[https://github.com/zc62/mpv-scripts/blob/master/autoloop.lua][autoloop.lua]]
+ [[https://github.com/occivink/mpv-scripts#blur-edgeslua][blur-edges.lua]]
