-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local awful = require("awful")
local gears = require("gears")
require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")
local shapes = require("shapes")
beautiful.init(gears.filesystem.get_configuration_dir() .. "theme.lua")

-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Volume widget
--local volume_widget = require('awesome-wm-widgets.volume-widget.volume')

-- Battery widget
--local battery_widget = require('awesome-wm-widgets.battery-widget.battery')

-- mpd widget
--require("widgets.mpd_status")

local globalkeys = awful.util.table.join()

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Oops, there were errors during startup!",
        text = awesome.startup_errors
    })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function(err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({
            preset = naughty.config.presets.critical,
            title = "Oops, an error happened!",
            text = tostring(err)
        })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.

-- This is used later as the default terminal and editor to run.
local editor = os.getenv("EDITOR") or "nvim"

-- Default modkey
local modkey = "Mod4"

-- My Variables
local BROWSER = os.getenv("BROWSER") or "firefox"
local XDG_CONFIG_HOME = os.getenv("XDG_CONFIG_HOME") or "~/.config"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.floating,
    --  awful.layout.suit.tile.left,
    --  awful.layout.suit.tile.bottom,
    --  awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    --  awful.layout.suit.fair.horizontal,
    --  awful.layout.suit.spiral,
    --  awful.layout.suit.spiral.dwindle,
    -- awful.layout.suit.max,
    awful.layout.suit.magnifier,
    awful.layout.suit.max.fullscreen,
    --  awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
    awful.button({}, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end)
)

local tasklist_buttons = gears.table.join(
    awful.button({}, 1, function(c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal(
                "request::activate",
                "tasklist",
                { raise = true }
            )
        end
    end),
    awful.button({}, 3, function()
        awful.menu.client_list({ theme = { width = 250 } })
    end),
    awful.button({}, 4, function()
        awful.client.focus.byidx(1)
    end),
    awful.button({}, 5, function()
        awful.client.focus.byidx(-1)
    end))

local tag_steam = ""
local tag_browser = ""
local tag_media_player = ""
local tagGames = "6"


awful.screen.connect_for_each_screen(function(s)
    -- Each screen has its own tag table.
    if s.index == 1 then
        awful.tag.new({ tag_media_player, "2", tag_browser, "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])
        awful.tag.add("steam", {
            name = tag_steam,
            index = 10,
            screen = s,
            layout = awful.layout.layouts[1]
        })
    elseif s.index == 2 then
        awful.tag.new({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])
    end

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
        awful.button({}, 1, function() awful.layout.inc(1) end),
        awful.button({}, 3, function() awful.layout.inc(-1) end),
        awful.button({}, 4, function() awful.layout.inc(1) end),
        awful.button({}, 5, function() awful.layout.inc(-1) end)))

    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        buttons         = taglist_buttons,
        screen          = s,
        filter          = awful.widget.taglist.filter.all,
        layout          = {
            spacing = 0,
            layout = wibox.layout.fixed.horizontal
        },
        style           = {
            shape = shapes.bottom_half_round_rect(6),
        },
        widget_template = {
            {
                {
                    widget = wibox.widget.textbox,
                    id = 'text_role',
                },
                widget = wibox.container.place,
                halign = "center",
            },
            widget = wibox.container.background,
            id = 'background_role',
            forced_width = 20,
        }
    }

    -- Create a tasklist widget
    s.tasklist = awful.widget.tasklist {
        screen          = s,
        filter          = awful.widget.tasklist.filter.currenttags,
        buttons         = tasklist_buttons,
        style           = { shape = shapes.bottom_half_round_rect(6) },
        layout          = {
            layout = wibox.layout.flex.horizontal,
            spacing = 0,
        },
        widget_template = {
            {
                {
                    {
                        {
                            {
                                id     = 'icon_role',
                                widget = wibox.widget.imagebox,
                            },
                            widget  = wibox.container.margin,
                            margins = 2,
                        },
                        {
                            {
                                widget = wibox.widget.textbox,
                                id     = 'text_role',
                            },
                            layout = wibox.container.scroll.horizontal,
                            step_function = wibox.container.scroll.step_functions.waiting_nonlinear_back_and_forth,
                            max_size = 600,
                            speed = 40
                        },
                        layout = wibox.layout.fixed.horizontal,
                    },
                    widget = wibox.container.place,
                    halign = "center"
                },
                widget = wibox.container.margin,
                left   = 10,
                right  = 10,
            },
            widget        = wibox.container.background,
            id            = 'background_role',
            forced_width  = 600,
            forced_height = 24,
        }
    }

    -- Textclock
    textclock = {
        {
            widget = wibox.widget.textclock("%d/%m/%Y %H:%M")
        },
        widget = wibox.container.background,
        bg = beautiful.background_colour
    }

    -- Separator
    separator = {
        {
            widget = wibox.widget.textbox,
            text = "|",
            forced_width = 8,
            color = "#ffffff",
        },
        widget = wibox.container.background,
        bg = beautiful.background_colour
    }
    --[[
    volumeConf = volume_widget { step = 1,
        device = pipewire
    }
]]
    --[[Battery
    batteryConf = battery_widget {
        show_current_level = false,
        display_notification = true,
        warning_msg_position = "top_right",
        enable_battery_warning = false,
    }
]]
    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        -- Left widgets
        {
            {
                layout = wibox.layout.fixed.horizontal,
                s.mytaglist,
                s.mypromptbox,
            },
            widget = wibox.container.background,
            shape = shapes.bottom_half_round_rect(6),
            bg = beautiful.wibar_back,
        },
        -- Middle widgets
        {
            {
                layout = wibox.layout.flex.horizontal,
                max_widget_size = nil,
                s.tasklist,
            },
            widget = wibox.container.place,
            halign = "center"
        },
        -- Right widgets
        {
            {
                {
                    layout = wibox.layout.fixed.horizontal,
                    mykeyboardlayout,
                },
                layout = wibox.layout.fixed.horizontal,
                separator,
                {
                    layout = awful.widget.only_on_screen,
                    screen = 1,
                    wibox.widget.systray(),
                },
                {
                    layout = awful.widget.only_on_screen,
                    screen = 1,
                    separator,
                },
                textclock,
                separator,
                --volumeConf,
                separator,
                --batteryConf,
                separator,
                s.mylayoutbox,
            },
            widget = wibox.container.background,
            shape = shapes.bottom_half_round_rect(6),
            bg = beautiful.background_colour,
        },
    }
end)
-- }}}

local programs = {
    terminal = "alacritty",
    filemanager = "pcmanfm",
    browser = BROWSER or "firefox"
}

local groups = {
    system = "System",
    programs = "Programs"
}

-- {{{ Keybinds
globalkeys = gears.table.join(
    awful.key({ modkey, }, "s", hotkeys_popup.show_help,
        { description = "show help", group = "awesome" }),
    awful.key({ modkey, }, "Left", awful.tag.viewprev,
        { description = "view previous", group = "tag" }),
    awful.key({ modkey, }, "Right", awful.tag.viewnext,
        { description = "view next", group = "tag" }),
    awful.key({ modkey, }, "Escape", awful.tag.history.restore,
        { description = "go back", group = "tag" }),
    awful.key({ modkey, }, "-", awful.tag.viewprev,
        { description = "view previous", group = "tag" }),
    awful.key({ modkey, }, "=", awful.tag.viewnext,
        { description = "view next", group = "tag" }),

    -- Toggle wibox
    awful.key({ modkey, "Shift" }, "b", function()
        mouse.screen.mywibox.visible = not mouse.screen.mywibox.visible
    end),

    awful.key({ modkey, }, "j",
        function()
            awful.client.focus.byidx(1)
        end,
        { description = "focus next by index", group = "client" }
    ),
    awful.key({ modkey, }, "k",
        function()
            awful.client.focus.byidx(-1)
        end,
        { description = "focus previous by index", group = "client" }
    ),

    -- Layout manipulation
    awful.key({ modkey, "Shift" }, "j", function() awful.client.swap.byidx(1) end,
        { description = "swap with next client by index", group = "client" }),
    awful.key({ modkey, "Shift" }, "k", function() awful.client.swap.byidx(-1) end,
        { description = "swap with previous client by index", group = "client" }),
    awful.key({ modkey, "Control" }, "j", function() awful.screen.focus_relative(1) end,
        { description = "focus the next screen", group = "screen" }),
    awful.key({ modkey, "Control" }, "k", function() awful.screen.focus_relative(-1) end,
        { description = "focus the previous screen", group = "screen" }),
    awful.key({ modkey, }, "u", awful.client.urgent.jumpto,
        { description = "jump to urgent client", group = "client" }),
    awful.key({ modkey, }, "Tab",
        function()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        { description = "go back", group = "client" }),

    -- Standard program
    awful.key({ modkey, }, "q", awesome.restart,
        { description = "reload awesome", group = "awesome" }),
    awful.key({ modkey, "Shift" }, "q", awesome.quit,
        { description = "quit awesome", group = "awesome" }),
    awful.key({ modkey, }, "l", function() awful.tag.incmwfact(0.05) end,
        { description = "increase master width factor", group = "layout" }),
    awful.key({ modkey, }, "h", function() awful.tag.incmwfact(-0.05) end,
        { description = "decrease master width factor", group = "layout" }),
    awful.key({ modkey, "Shift" }, "h", function() awful.tag.incnmaster(1, nil, true) end,
        { description = "increase the number of master clients", group = "layout" }),
    awful.key({ modkey, "Shift" }, "l", function() awful.tag.incnmaster(-1, nil, true) end,
        { description = "decrease the number of master clients", group = "layout" }),
    awful.key({ modkey, "Control" }, "h", function() awful.tag.incncol(1, nil, true) end,
        { description = "increase the number of columns", group = "layout" }),
    awful.key({ modkey, "Control" }, "l", function() awful.tag.incncol(-1, nil, true) end,
        { description = "decrease the number of columns", group = "layout" }),
    awful.key({ modkey, }, "space", function() awful.layout.inc(1) end,
        { description = "select next", group = "layout" }),
    awful.key({ modkey, "Shift" }, "space", function() awful.layout.inc(-1) end,
        { description = "select previous", group = "layout" }),

    awful.key({ modkey, "Control" }, "n",
        function()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal(
                    "request::activate", "key.unminimize", { raise = true }
                )
            end
        end,
        { description = "restore minimized", group = "client" }),

    -- Run Launcher
    awful.key({ modkey }, "r", function()
            awful.spawn("dmenu_run -i -m 0 -h 24 -p 'Run:' -l 10")
        end,
        { description = "Run dmenu", group = "launcher" }),

    awful.key({ modkey, "Shift" }, "x", function()
            awful.spawn.with_shell("dm_power -i -m 0 -h 24 -l 10")
        end,
        { description = "Run power manager", group = "launcher" }),

    awful.key({ modkey }, "x",
        function()
            awful.prompt.run {
                prompt       = "Run Lua code: ",
                textbox      = awful.screen.focused().mypromptbox.widget,
                exe_callback = awful.util.eval,
                history_path = awful.util.get_cache_dir() .. "/history_eval",
            }
        end,
        { description = "lua execute prompt", group = "awesome" }),
    -- Menubar
    awful.key({ modkey, "Control" }, "p", function() menubar.show() end,
        { description = "show the menubar", group = "launcher" }),

    -- Audio control
    awful.key({}, "XF86AudioRaiseVolume", function() awful.spawn.with_shell("amixer set Master 2%+ --quiet") end,
        { description = "Increase volume", group = groups.system }
    ),
    awful.key({}, "XF86AudioLowerVolume", function() awful.spawn.with_shell("amixer set Master 2%- --quiet") end,
        { description = "Decrease volume", group = groups.system }
    ),
    awful.key({}, "XF86AudioMute", function() awful.spawn.with_shell("amixer set Master toggle --quiet") end,
        { description = "Mute/Unmute sound", group = groups.system }
    ),
    awful.key({}, "XF86AudioMicMute", function() awful.spawn.with_shell("amixer set Capture toggle --quiet") end,
        { description = "Mute/Unmute mic", group = groups.system }
    ),

    -- Brightness control
    awful.key({}, "XF86MonBrightnessUp", function() awful.spawn.with_shell("xbacklight -inc 10% -time 0") end,
        { description = "Increase brightness", group = groups.system }
    ),
    awful.key({}, "XF86MonBrightnessDown", function() awful.spawn.with_shell("xbacklight -dec 10% -time 0") end,
        { description = "Decrease brightness", group = groups.system }
    ),

    -- Programs
    awful.key({ modkey, }, "Return", function() awful.spawn(programs.terminal) end,
        { description = "Launch terminal", group = groups.programs }),
    awful.key({ modkey, "Control" }, "v", function() awful.spawn.with_shell(programs.filemanager) end,
        { description = "Open GUI filemanager", group = groups.programs }),
    awful.key({ modkey, "Control" }, "b", function() awful.spawn.with_shell(programs.browser) end,
        { description = "Open Browser", group = groups.programs }),
    awful.key({ modkey }, "Print",
        function() awful.spawn.with_shell("flameshot screen --number 1 -c -p $HOME/Pictures/Screenshots/") end,
        { description = "Screenshot primary monitor", group = groups.programs }),
    awful.key({ modkey, "Shift" }, "Print",
        function() awful.spawn.with_shell("flameshot gui -c -p $HOME/Pictures/Screenshots/") end,
        { description = "Select region to screenshot", group = groups.programs })

)

clientkeys = gears.table.join(
    awful.key({ modkey, }, "f",
        function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        { description = "toggle fullscreen", group = "client" }),
    awful.key({ modkey, }, "c", function(c) c:kill() end,
        { description = "close", group = "client" }),
    awful.key({ modkey, "Control" }, "space", awful.client.floating.toggle,
        { description = "toggle floating", group = "client" }),
    awful.key({ modkey, "Control" }, "Return", function(c) c:swap(awful.client.getmaster()) end,
        { description = "move to master", group = "client" }),
    awful.key({ modkey, }, "o", function(c) c:move_to_screen() end,
        { description = "move to screen", group = "client" }),
    awful.key({ modkey, }, "t", function(c) c.ontop = not c.ontop end,
        { description = "toggle keep on top", group = "client" }),
    awful.key({ modkey, }, "n",
        function(c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end,
        { description = "minimize", group = "client" }),
    awful.key({ modkey, }, "m",
        function(c)
            c.maximized = not c.maximized
            c:raise()
        end,
        { description = "(un)maximize", group = "client" }),
    awful.key({ modkey, "Control" }, "m",
        function(c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end,
        { description = "(un)maximize vertically", group = "client" }),
    awful.key({ modkey, "Shift" }, "m",
        function(c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end,
        { description = "(un)maximize horizontally", group = "client" }),
    awful.key({ modkey, "Control" }, "t", awful.titlebar.toggle,
        { description = "toggle titlebar", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 10 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            { description = "view tag #" .. i, group = "tag" }),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            { description = "toggle tag #" .. i, group = "tag" }),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end,
            { description = "move focused client to tag #" .. i, group = "tag" }),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            { description = "toggle focused client on tag #" .. i, group = "tag" })
    )
end

clientbuttons = gears.table.join(
    awful.button({}, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ modkey }, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

--------#############--------
--------#   Rules   #--------
--------#############--------


-- Rules to apply to new clients
awful.rules.rules = {
    {
        rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true, -- I have no idea what this does if anything at all
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_offscreen,
        }
    },

    -- Floating clients --
    {
        rule_any = { class = { "Pavucontrol" } },
        properties = { floating = true }
    },

    {
        rule_any = { class = { "KeePassXC" } },
        properties = { floating = false, placement = "centered" }
    },

    {
        rule = { type = "dialog", },
        properties = { floating = true }
    },

    {
        rule = { role = "Organizer", },
        properties = { floating = true, placement = "centered" }
    },

    -- Just general rules for whatever --
    {
        rule_any = { instance = { "yt-dlp", "wget", "qbittorrent" } },
        properties = { tag = "7", screen = 1 }
    },

    {
        rule = { class = "discord" },
        properties = { tag = "9", screen = 1 }
    },

    {
        rule_any = { class = { "Brave-browser", "qutebrowser", "firefox" } },
        properties = { tag = tag_browser, screen = 1 }
    },

    {
        rule = { class = "mpv" },
        properties = { tag = tag_media_player, switch_to_tags = false, screen = 1 },
        callback = function(c)
            awful.spawn.with_shell("mpc pause")
        end
    },

    {
        rule = { class = "Pavucontrol" },
        properties = {
            placement = "top_right",
            ontop = true,
            sticky = true,
            width = 720,
            height = 480,
        }
    },

    {
        rule = { class = "Nsxiv" },
        properties = { fullscreen = true }
    },

    {
        rule = { class = "Element" },
        properties = { tag = "8" }
    },


    -- Rules for Chatty clients --
    {
        rule = { class = "chatty-Chatty" },
        properties = { tag = tag_media_player },
        callback = awful.client.setslave
    },

    {
        rule = { instance = "chatty-Chatty", name = "Emoticons%s.+" },
        properties = { tag = tag_media_player, floating = true, screen = 1, placement = "right" }
    },

    {
        rule = { instance = "chatty-Chatty", name = "User:%s.+" },
        properties = {
            tag = tag_media_player,
            floating = true,
            placement = "top_right"
        }
    },

    -- Rules for Steam clients (not complete, some stuff doesn't seem to work consistently) --
    {
        rule = { class = "steam" },
        properties = { tag = tag_steam, maximized = false, screen = 1, floating = false }
    },

    {
        rule = { class = "steam", name = "Steam -.+" },
        properties = { tag = tag_steam, maximized = false, screen = 1, floating = true }
    },

    {
        rule = { class = "steam", name = ".+- Steam" },
        properties = { tag = "3", maximized = false, screen = 1, placement = "right", floating = true }
    },

    {
        rule = { class = "steam", name = "Updating.+" },
        properties = { tag = "3", maximized = false, screen = 1, placement = "right", floating = true }
    },

    {
        rule = { class = "steam", name = "Install -.+" },
        properties = { tag = tag_steam, maximized = false, screen = 1, placement = "centered", floating = true }
    },

    {
        rule = { class = "steam", name = "Properties -.+" },
        properties = { tag = tag_steam, maximized = false, screen = 1, placement = "centered", floating = true }
    },

    {
        rule = { class = "steam", name = "Steam Settings" },
        properties = { tag = tag_steam, maximized = false, screen = 1, placement = "centered", floating = true }
    },

    {
        rule = { class = "steam", name = "Friends List" },
        properties = { tag = tag_steam, maximized = false, screen = 1, floating = true, }
    },

    {
        rule = { class = "steam", name = "Screenshot Uploader" },
        properties = { tag = tag_steam, maximized = false, screen = 1, placement = "centered", floating = true }
    },

    {
        rule = { class = "steam", name = "Screenshot" },
        properties = { tag = tag_steam, maximized = false, screen = 1, placement = "centered", floating = true }
    },

    -- Games
    -- For games ran trough Steam with Proton
    {
        rule = { class = "steam_app_*" },
        properties = { tag = tagGames, maximized = false, fullscreen = true, floating = false, screen = 1 }
    },

    -- For native games
    {
        rule_any = { class = { "factorio", "hollow_knight.x86_64", "fury.bin", "Project Zomboid" } },
        properties = { tag = tagGames, screen = 1 }
    },

}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
        and not c.size_hints.user_position
        and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({}, 1, function()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            awful.mouse.client.move(c)
        end),
        awful.button({}, 3, function()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c):setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        {     -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton(c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton(c),
            awful.titlebar.widget.ontopbutton(c),
            awful.titlebar.widget.closebutton(c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

naughty.config.defaults.screen = 1

beautiful.systray_icon_spacing = 1

awful.spawn.with_shell(XDG_CONFIG_HOME .. "/awesome/autorun.sh")

gears.timer {
       timeout = 30,
       autostart = true,
       callback = function() collectgarbage() end
}
