local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local wibox = require("wibox")
local naughty = require("naughty")
local shapes = require("shapes")

local box_values = {}
box_values.x = 1538
box_values.y = 36
box_values.width = 370
box_values.height = 100

local function box(x, y, width, height)
  return wibox{
      visible = true,
      ontop = false,
      x = x or 0,
      y = y or 0,
      width = width or 370,
      height = height or 100,
      bg = beautiful.background_colour,
      shape = shapes.round_rect(6)
  }
end

local mpd_widget_box = box(box_values.x, box_values.y, box_values.width, box_values.height)

local metadata = {}
local mpd_status_widget = wibox.widget{
    widget = wibox.widget.textbox,
    markup = "Playing: "
}

local get_mpd_status_cmd = "mpc status"
local function get_mpd_status()
  awful.spawn.easy_async(get_mpd_status_cmd, function(stdout, _, _, _)
                           local current_song = string.match(stdout, "[^\r\n]+")
                           --For some reason text isn't rendered when it contains "&", seems to be a bug with Awesome unless there's a reason for it, for now I put this workaround here that replaces "&" with "and"
                           if string.match(current_song, "&")
                           then current_song = string.gsub(current_song, "&", "and")
                           end

                           local mpd_status = string.match(stdout, "%[(.*)%]")
                           if mpd_status == "playing"
                           then
                             mpd_status_widget.markup = current_song
                           elseif mpd_status == "paused"
                           then
                             mpd_status_widget.markup = current_song
                           else
                             mpd_status_widget.markup = "stopped"
                           end
  end)
end

local cover_art = wibox.widget{
  widget = wibox.widget.imagebox,
  image = "/home/bloody/Pictures/spinning-swimming.gif"
}

mpd_widget_box:setup{
  widget = wibox.container.margin,
  margins = 4,
  {
    layout = wibox.layout.fixed.horizontal,
    spacing = 4,
    {
      widget = wibox.container.background,
      --bg = "#ff0000",
      forced_width = box_values.height,
      forced_height = box_values.height,
      {
        layout = wibox.layout.fixed.horizontal,
        cover_art,
      }
    },
    {
      layout = wibox.container.place,
      valign = "top",
      {
        widget = wibox.container.background,
        --bg = "#ff0000",
        {
          {
            layout = wibox.layout.flex.horizontal,
            mpd_status_widget
          },
          layout = wibox.container.constraint,
          width = box_values.width - box_values.height,
          --height = string.match(beautiful.font, "%d+") + math.ceil(string.match(beautiful.font, "%d+") * 0.66)
        }
      }
    }
  }
}

awful.widget.watch(get_mpd_status_cmd, 1, get_mpd_status)

return mpd_widget_box
