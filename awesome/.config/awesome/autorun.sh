#!/bin/sh

run() {
  if ! pgrep -f "$1" ;
  then
    $@&
  fi
}

# System specific, change as needed
run xrandr --output HDMI-0 --primary --mode 1920x1080 --pos 1920x0 --rotate normal --rate 144 --output eDP-1-1 --mode 1920x1080 --pos 0x0 --rotate normal
run setxkbmap gb,cz

# Sound server
run qpwgraph -am              # Pipewire patchbay

# Compositor
#run picom --config "$HOME/.config/picom/picom.conf"
run picom -b

# Notification daemon
#run dunst

# Some programs I like to autostart
run nitrogen --restore
run nm-applet
run flameshot                 # Screenshot utility
run keepassxc                 # Password manager
run qbittorrent
run flatpak run com.discordapp.Discord
run steam
