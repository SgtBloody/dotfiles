local gears = require("gears")
local shapes = {}

function shapes.bottom_half_round_rect(radius)
  return function(cr, width, height)
    gears.shape.partially_rounded_rect(cr, width, height, false, false, true, true, radius)
  end
end

function shapes.round_rect(radius)
  return function(cr, width, height)
    gears.shape.rounded_rect(cr, width, height, radius)
  end
end

return shapes
