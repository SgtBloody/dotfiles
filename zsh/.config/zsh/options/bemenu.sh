# These are options I like to have set as a default

foreground="#bbc2cf"
background="#282c34"
highlights="#b30059"
font="NotoSans Nerd Font:size=11"

export BEMENU_OPTS="--tb $highlights --nb $background --fb $background --hb $highlights --ab $background --nf $foreground --hf $foreground --sf $foreground --tf $foreground --ff $foreground --af $foreground --fn $font --ignorecase -H 24 --hb 6"
