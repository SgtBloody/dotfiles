#-*- mode: sh; -*-

export GPG_TTY=$(tty)

### Sourcing ###
# Aliases
[ -f "$XDG_CONFIG_HOME/zsh/aliases" ] && source "$XDG_CONFIG_HOME/zsh/aliases"

export EXA_COLORS="xx=37"

# Plugins
[ -f "/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ] && source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2> /dev/null
[ -f "/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh" ] && source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2> /dev/null

# History
HISTFILE="$XDG_CONFIG_HOME/zsh/history"
HISTSIZE=10000
SAVEHIST=100000000000
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_ALL_DUPS

# Basic auto/tab complete:
autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit -d "$XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION"
_comp_options+=(globdots)       # Include hidden files.

# Vi mode
bindkey -v
export KEYTIMEOUT=1

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Vim-like keybinds for Tab completion menu
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Visual cd using ranger
rangercd () {
    tmp="$(mktemp)"
    ranger --choosedir="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
bindkey -s '^o' 'rangercd\n'

### Highlighting ###
typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]='fg=#00e80a'
ZSH_HIGHLIGHT_STYLES[single-quoted-argument-unclosed]='fg=#ff0000'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]='fg=#00e80a'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument-unclosed]='fg=#ff0000'
ZSH_HIGHLIGHT_STYLES[builtin]='fg=#ffa366'
ZSH_HIGHLIGHT_STYLES[command]='fg=#3333ff'
ZSH_HIGHLIGHT_STYLES[alias]='fg=#ff3399'

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

# opam configuration
[[ ! -r /home/bloody/.opam/opam-init/init.zsh ]] || source /home/bloody/.opam/opam-init/init.zsh  > /dev/null 2> /dev/null

[ -f "/usr/bin/starship" ] && eval "$(starship init zsh)" # Starship prompt
[ -f "/usr/local/bin/luacowsay" ] && luacowsay "dingus"
