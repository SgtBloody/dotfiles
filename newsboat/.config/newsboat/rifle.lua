#!/usr/bin/lua
---@type string
local argument = arg[1]
if not argument then
    io.write("No URL provided\n")
    os.execute("notify-send -t 5000 \"No URL provided\" &")
    os.exit()
end

---@param url string
---@return boolean
local function urlmatch(url)
    return string.match(argument, url)
end

local quiet = " > /dev/null 2>&1 &" ---@type string
local secondHalf = argument .. quiet ---@type string

--Tables within table to keep priority
---@type table<number, table<string, string>>
local urlMatches = {
    { [".*jpe?g$"] = "nsxiv-url " },
    { [".*png$"] = "nsxiv-url " },
    { [".*gif$"] = "nsxiv-url " },
    { [".*mp4$"] = "mpv " },
    { ["^https://.*watch%?v=.*"] = "mpv " },
    { ["^https://.*"] = "$BROWSER " },
    { [".*"] = "$BROWSER " }
}

---@type integer, integer
local noMatchMax, noMatch = #urlMatches, 0

for k,_ in ipairs(urlMatches) do
    for paternMatch,command in pairs(urlMatches[k]) do
        if argument == urlmatch(paternMatch)
        then
            io.write("Executing ", command, secondHalf, "\n")
            os.execute(command .. secondHalf)
            os.exit()
        else
            noMatch = noMatch + 1
        end
    end

    if noMatchMax == noMatch then
        io.write("Couldn't match URL: ", argument, "\n")
        os.execute("notify-send -t 5000 \"" .. noMatch .. "\" &")
    end
end
