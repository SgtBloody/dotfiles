#!/bin/sh
set -eu

case "$@" in
    *png|*jpg|*gif)
        nsxiv-url "$@" > /dev/null 2>&1 &;;
    https://*watch\?v=*)
        mpv "$@" > /dev/null 2>&1 &;;
    https://*)
        "$BROWSER" "$@" > /dev/null 2>&1 &;;
    *)
        echo "Error" & notify-send -t 5000 "Error" &;;
esac
