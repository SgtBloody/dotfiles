#!/bin/sh

run() {
  if ! pgrep -f "$1" ;
  then
    $@&
  fi
}

# System specific, change as needed
run xrandr --output HDMI-0 --primary --mode 1920x1080 --pos 1920x0 --rotate normal --rate 144 --output eDP-1-1 --mode 1920x1080 --pos 0x0 --rotate normal
#run xcompmgr -cfCF -D 4 -r 4 -o 1 -t -5 -l -5 > /dev/null 2>&1

# Sound server
run dbus-run-session pipewire
run pipewire-pulse
run qpwgraph -am              # Pipewire patchbay

# Compositor
#run picom --config "$HOME/.config/picom/picom.conf"
#xcompmgr -cfCF -D 4 -r 4 -o 1 -t -5 -l -5 > /dev/null 2>&1

# Notification daemon
run dunst

# Some programs I like to autostart
run nitrogen --restore
run flameshot                 # Screenshot utility
run keepassxc                 # Password manager  NOTE: KeePassXC has problems with spectrwm
run qbittorrent
run flatpak run com.discordapp.Discord
run steam
