local o = vim.o -- Wrapped function for :set
--local go = vim.go -- Set global options
--local wo = vim.wo -- Set window-scoped options
--local bo = vim.bo -- Set buffer-scoped options

local function set_options(scope, options)
    for k,v in pairs(options) do
        scope[k] = v
    end
end

vim.g.mapleader = " "
local options_global = {
    -- User interface
    number = true, -- Enables line numbers
    relativenumber = true, -- Makes line numbers relative to the selected line
    signcolumn = "yes:1", -- Enables signs column and sets it's width 1
    splitbelow = true, -- Force horizontal splits to go below current buffer
    splitright = true, -- Force vertical splits to go to the right of current buffer
    cursorline = true, -- Highlights cursor
    spell = true, -- Enables spell checking
    spelllang = "en_gb", -- Spell checking language
    hlsearch = true, -- Highlights search
    incsearch = true, -- Highlights search incrementally
    mouse = "a", -- Enables mouse

    -- Text rendering
    linebreak = true, -- Wrap lines at the 'breakat' characters
    scrolloff = 12, -- Scrolling starts set number of lines before end of buffer
    termguicolors = true, -- Enables truecolor support
    list = true, -- Display trailing whitespace, tabs
    listchars = "tab:> ,trail:█,nbsp:+",

    -- Indentation
    autoindent = true, -- Copy indentation from current line to next line when typing new line
    smartindent = true, -- Auto indentation for languages
    expandtab = true, -- Sets spaces for indentation
    shiftwidth = 4, -- Number of space characters for auto indentation
    tabstop = 4,    -- Width of tabs

    -- Text search
    ignorecase = true, -- Forces search to ignore casing

    -- Other
    clipboard = "unnamedplus", -- Copy/Paste between Neovim and System
    autochdir = false, -- Auto change directory
    updatetime = 250,
}

set_options(o, options_global)
if vim.g.neovide then
    local optionsNeovide = {
        guifont = "FiraCode Nerd Font:h10,Twemoji:h10", -- Sets font for GUI clients
    }
    set_options(o, optionsNeovide)
end
