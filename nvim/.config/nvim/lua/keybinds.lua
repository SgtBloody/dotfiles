local wk = require("which-key")

local nvimHome = os.getenv("HOME") .. "/.config/nvim/"

---@param state boolean
---@return boolean
local function toggleBool(state)
    if state then
        state = false
    else
        state = true
    end
    return state
end

local function toggleSpell()
    return function() vim.o.spell = toggleBool(vim.o.spell) end
end

-- Normal mode keybinds
wk.register {
    -- Moving between splits
    ["<C-h>"] = { "<C-w>h", "Move to left split" },
    ["<C-j>"] = { "<C-w>j", "Move to bottom split" },
    ["<C-k>"] = { "<C-w>k", "Move to top split" },
    ["<C-l>"] = { "<C-w>l", "Move to right split" },

    -- Moving splits
    ["<M-h>"] = { "<C-w>H", "Move split to the left" },
    ["<M-j>"] = { "<C-w>J", "Move split to the bottom" },
    ["<M-k>"] = { "<C-w>K", "Move split to the top" },
    ["<M-l>"] = { "<C-w>L", "Move split to the right" },

    ["<leader>"] = {
        -- Buffer management
        b = {
            name = "buffer",
            b = { function() require("telescope.builtin").buffers() end, "Show Buffers" },
            d = { ":bdelete<CR>", "Delete Buffer" },
            h = { ":bprevious<CR>", "Previous Buffer" },
            l = { ":bnext<CR>", "Next Buffer" },
        },

        c = {
            name = "code",
            l = {
                name = "lsp",
                i = { ":LspInfo<CR>", "Info about attached servers" },
                s = { ":LspStart<CR>", "Start language server" },
                r = { ":LspRestart<CR>", "Restart language server" },
                d = { ":LspStop<CR>", "Stop language server" }
            }
        },

        f = {
            name = "file",
            f = { function() require("telescope.builtin").find_files() end, "Find file" },
        },

        o = {
            name = "open",
            h = { function() require("telescope.builtin").help_tags() end, "Search help documentation" },
            m = { function() require("telescope.builtin").man_pages() end, "Search Man pages" },
            n = { function() require("neogit").open() end, "Open Neogit" },
            f = { function() require("nvim-tree.api").tree.toggle() end, "Open/Close Nvim-Tree" },
            C = {
                name = "config",
                c = { function() require("nvim-tree.api").tree.open { path = nvimHome } end,
                    "Open Personal Configuration" },
                p = { function() vim.cmd(":edit " .. nvimHome .. "lua/plugins.lua") end, "Open plugins" },
                k = { function() vim.cmd(":edit " .. nvimHome .. "lua/keybinds.lua") end, "Open keybinds" },
            }
        },

        p = {
            name = "Plugin manager",
            S = { function() require("lazy").sync() end, "Install, clean and update plugins" },
            h = { function() require("lazy").home() end, "Show list of plugins" },
            l = { function() require("lazy").log() end, "Show update log" },
            c = { function() require("lazy").check() end, "Check for updates" },
            p = { function() require("lazy").profile() end, "Show detailed profiling" },
        },

        t = {
            name = "toggle options",
            s = { toggleSpell(), "Toggle spellchecking" }
        }
    },
}

-- Insert mode keybinds
wk.register({
    ["<M-CR>"] = { "<Esc>o", "Insert new line" },
}, { mode = "i" })

-- Visual mode keybinds
wk.register({
    ["<"] = { "<gv", "Move indentation" },
    [">"] = { ">gv", "Move indentation" },

    p = { '"_dP' } -- Makes it so that replaced text won't change the clipboard NOTE: Doesn't seem to work
}, { mode = "v" })

-- Terminal mode keybinds
wk.register({
    ["<ESC>"] = { "<C-\\><C-n>", "Exit from terminal" }
}, { mode = "t" })

-- Keybinds for Neovide
if vim.g.neovide
then
    local function changeScaleFactor(delta)
        return function() vim.g.neovide_scale_factor = vim.g.neovide_scale_factor + delta end
    end
    wk.register {
        ["<C-->"] = { changeScaleFactor(-0.1), "Scale down" },
        ["<C-=>"] = { changeScaleFactor(0.1), "Scale up" }
    }
end

--[[ Binds for switching between splits with CTRL+hjkl
keymap('n', '<C-h>', '<C-w>h', {})
keymap('n', '<C-j>', '<C-w>j', {})
keymap('n', '<C-k>', '<C-w>k', {})
keymap('n', '<C-l>', '<C-w>l', {})


-- Binds for indenting
keymap('v', '<', '<gv', {})
keymap('v', '>', '>gv', {})
keymap('v', 'p', '"_dP', {})

-- Buffer management
keymap('n', '<Leader>bb', ':Telescope buffers<CR>', { desc = "Show Buffers" }) -- List all buffers
keymap('n', '<Leader>bl', ':bnext<CR>', { desc = "Next Buffer" }) -- Switch to next buffer
keymap('n', '<Leader>bh', ':bprevious<CR>', { desc = "Previous Buffer" }) -- Switch to previous buffer
keymap('n', '<Leader>bd', ':bdelete<CR>', { desc = "Delete Buffer" }) -- Delete current buffer

-- Other
keymap('n', '<Leader>oo', ':NvimTreeToggle<CR>', { silent = true, desc = "Toggle Nvim-Tree" }) -- Open nvim-tree

-- Telescope
keymap('n', '<Leader>of', ':Telescope find_files<CR>', { desc = "Open Telescope" })

-- Bindings for Dashboard in setup/dashboard.lua
keymap('n', '<Leader>fP', ':NvimTreeOpen ' .. home .. '/.config/nvim<CR>',
    { silent = true, desc = "Open Personal Configuration" })
]]
