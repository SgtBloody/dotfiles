return {
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        main = "nvim-treesitter.configs",
        event = { "BufReadPost", "BufNewFile" },
        dependencies = { "HiPhish/nvim-ts-rainbow2" },

        opts = {
            ensure_installed = { "lua", "bash", "html", "css", "astro", "git_config", "gitignore", "gitcommit",
                "make", "markdown", "org", "sxhkdrc", "vim" },
            sync_install = false,
            auto_install = true,
            ignore_install = {},
            highlight = {
                enable = true,
                disable = { "astro" },
                additional_vim_regex_highlighting = false
            },
            indent = {
                enable = true,
                disable = { "astro" }
            },
            incremental_selection = {
                enable = true
            },
            rainbow = {
                enable = true,
            }
        },

    },

    {
        "HiPhish/nvim-ts-rainbow2",
        lazy = true,
    }
}
