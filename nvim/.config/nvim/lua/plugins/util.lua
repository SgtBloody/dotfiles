return {
    {
        "NeogitOrg/neogit",
        cmd = "Neogit",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-telescope/telescope.nvim",
            "sindrets/diffview.nvim",
            "ibhagwan/fzf-lua"
        },

        opts = {}
    },

    {
        "jakewvincent/mkdnflow.nvim",
        event = { "BufEnter *.md" },
        dependencies = "folke/which-key.nvim",


        opts = function(_, _)
            local wk = require("which-key")
            local modes = { "n", "v" }
            for _, v in ipairs(modes) do
                wk.register({
                    ["<CR>"] = { "<Cmd>MkdnEnter<CR>", "Create/follow a link" },
                    ["<leader>m"] = {
                        p = { "<Cmd>MkdnCreateLinkFromClipboard<CR>", "Create link from content of clipboard" }
                    }
                }, { mode = v })
            end
            wk.register({
                ["<Tab>"] = { ":MkdnNextLink<CR>", "Goto next link" },
                ["<S-Tab>"] = { ":MkdnPrevLink<CR>", "Goto previous link" },
                ["]]"] = { ":MkdnNextHeading<CR>", "Goto next heading" },
                ["[["] = { ":MkdnPrevHeading<CR>", "Goto previous heading" },
                ["+"] = { ":MkdnIncreaseHeading<CR>", "Increase heading level" },
                ["-"] = { ":MkdnDecreaseHeading<CR>", "Decrease heading level" },
                ["<BS>"] = { ":MkdnGoBack<CR>", "Goto last buffer" },
                ["<Del>"] = { ":MkdnGoForward<CR>", "Goto ? buffer" },
                ["<C-Space>"] = { ":MkdnToggleToDo<CR>", "Toggle To-Do" },
                ["<M-CR>"] = { ":MkdnDestroyLink<CR>", "Delete the link and replace it with text" },
                o = { ":MkdnNewListItemBelowInsert<CR>", "Add new list item below" },
                O = { ":MkdnNewListItemAboveInsert<CR>", "Add new list item above" },

                ["<leader>m"] = {
                    name = "mkdnflow",
                    n = { ":MkdnUpdateNumbering<CR>", "Update numbering for the current list" },
                    y = {
                        name = "yank",
                        a = { ":MkdnYankAnchorLink<CR>", "Yank anchor link" },
                        f = { ":MkdnYankFileAnchorLink<CR>", "Yank anchor link with filename" }
                    },
                    t = {
                        name = "table",
                        n = { ":MkdnTable ncol nrow", "Make new table" },
                        f = { ":MkdnTableFormat<CR>", "Format table" },
                        r = { ":MkdnTableNewRowBelow<CR>", "Add new row below" },
                        R = { ":MkdnTableNewRowAbove<CR>", "Add new row above" },
                        c = { ":MkdnTableNewColAfter<CR>", "Add new column following after cursor" },
                        C = { ":MkdnTableNewColBefore<CR>", "Add new column following before cursor" }
                    }
                }
            }, { mode = "n" })

            wk.register({
                ["<M-CR>"] = { "<Cmd>MkdnTagSpan<CR>", "Tag selected text with ID" }
            }, { mode = "v" })

            wk.register({
                ["<Tab>"] = { "<Cmd>MkdnTableNextCell<CR>", "Jump to the next cell" },
                ["<S-Tab>"] = { "<Cmd>MkdnTablePrevCell<CR>", "Jump to the previous cell" }
            }, { mode = "i" })

            return {
                links = {
                    conceal = true
                },
                mappings = false
            }
        end,
    }
}
