return {
    { "kyazdani42/nvim-web-devicons", lazy = true },
    { "nvim-lua/plenary.nvim",        lazy = true },
    { "BurntSushi/ripgrep",           lazy = true },
    { "sharkdp/fd",                   lazy = true },
    { "sindrets/diffview.nvim",       lazy = true },
    { "ibhagwan/fzf-lua",             lazy = true },
}
