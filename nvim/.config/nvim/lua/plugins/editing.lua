return {
    {
        "nvim-telescope/telescope.nvim",
        cmd = "Telescope",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "BurntSushi/ripgrep",
            "sharkdp/fd",
        },

        opts = {
            defaults = {
                -- Default configuration for telescope goes here:
                border = true,
                borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
            },
            pickers = {
                -- Default configuration for builtin pickers goes here:
                find_files = {
                    initial_mode = "normal",
                    hidden = true,
                },
                buffers = {
                    initial_mode = "normal",
                }
            },
            extensions = {
                -- Your extension configuration goes here:
                -- extension_name = {
                --   extension_config_key = value,
                -- }
                -- please take a look at the readme of the extension you want to configure
            }
        }
    },

    {
        "NvChad/nvim-colorizer.lua",
        event = { "BufReadPost", "BufNewFile" },

        opts = {
            filetypes = { "*" },
            user_default_options = {
                RGB = true,          -- #RGB hex codes
                RRGGBB = true,       -- #RRGGBB hex codes
                names = true,        -- "Name" codes like Blue or blue
                RRGGBBAA = false,    -- #RRGGBBAA hex codes
                AARRGGBB = false,    -- 0xAARRGGBB hex codes
                rgb_fn = false,      -- CSS rgb() and rgba() functions
                hsl_fn = false,      -- CSS hsl() and hsla() functions
                css = false,         -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
                css_fn = false,      -- Enable all CSS *functions*: rgb_fn, hsl_fn
                -- Available modes for `mode`: foreground, background,  virtualtext
                mode = "background", -- Set the display mode.
                -- Available methods are false / true / "normal" / "lsp" / "both"
                -- True is same as normal
                tailwind = false,                                -- Enable tailwind colors
                -- parsers can contain values used in |user_default_options|
                sass = { enable = false, parsers = { "css" }, }, -- Enable sass colors
                virtualtext = "■",
            },
            -- all the sub-options of filetypes apply to buftypes
            buftypes = {},
        }
    },

    {
        "winston0410/range-highlight.nvim", -- Highlights range in command mode
        event = { "BufReadPost", "BufNewFile" },
        dependencies = { "winston0410/cmd-parser.nvim" },

        opts = {}
    },

    {
        "RRethy/vim-illuminate",
        event = { "BufReadPost", "BufNewFile" },

        opts = {},

        config = function(_, opts)
            require("illuminate").configure {}
        end
    },

    {
        "numToStr/Comment.nvim",
        event = { "BufReadPost", "BufNewFile" },

        opts = {}
    },

    {
        "windwp/nvim-autopairs", -- Automatic pairs for '(),"",...'
        enabled = false,
        event = "InsertEnter"
    },

    {
        "echasnovski/mini.pairs",
        event = { "BufReadPost", "BufNewFile" },

        opts = {}
    },

    {
        "kylechui/nvim-surround",
        event = { "BufReadPost", "BufNewFile" },

        opts = {}
    },

    {
        "folke/todo-comments.nvim",
        event = { "BufReadPost", "BufNewFile" },
        dependencies = {
            "nvim-lua/plenary.nvim",
            "BurntSushi/ripgrep",
        },

        opts = {}
    },

    {
        "mattn/emmet-vim",
        event = { "BufEnter *.html", "BufEnter *.css", "BufEnter *.astro" }
    },
}
