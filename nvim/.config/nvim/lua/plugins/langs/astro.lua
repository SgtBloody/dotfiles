return {
    {
        "wuelnerdotexe/vim-astro",
        event = "BufEnter *.astro",

        config = function ()
            vim.g.astro_typescript  = true
        end
    },

    {
        "mattn/emmet-vim",
        event = "BufEnter *.astro"
    },
}
