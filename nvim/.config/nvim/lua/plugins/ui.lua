local home = os.getenv("HOME")

return {
    {
        "nvim-lualine/lualine.nvim",
        dependencies = { "kyazdani42/nvim-web-devicons" },
        event = "VeryLazy",

        opts = {
            options = {
                icons_enabled = true,
                theme = "auto",
                component_separators = { left = '|', right = '|' },
                section_separators = { left = '', right = '' },
                disabled_filetypes = {
                    statusline = {},
                    winbar = {},
                },
                ignore_focus = {},
                always_divide_middle = true,
                globalstatus = true,
                refresh = {
                    statusline = 1000,
                    tabline = 1000,
                    winbar = 1000,
                }
            },
            sections = {
                lualine_a = { 'mode' },
                lualine_b = { 'branch', 'diff', 'diagnostics' },
                lualine_c = {
                    'filesize',
                    {
                        'filename',
                        path = 1,
                    }
                },
                lualine_x = { 'encoding', 'fileformat',
                    {
                        'filetype',
                        icon = { align = 'left' }
                    },
                },
                lualine_y = { 'progress' },
                lualine_z = { 'location' }
            },
            inactive_sections = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = { 'filename' },
                lualine_x = { 'location' },
                lualine_y = {},
                lualine_z = {}
            },
            tabline = {},
            winbar = {},
            inactive_winbar = {},
            extensions = {}
        }
    },

    {
        "kyazdani42/nvim-tree.lua",
        cmd = { "NvimTreeToggle", "NvimTreeOpen" },
        dependencies = { "kyazdani42/nvim-web-devicons" },

        opts = {
            hijack_cursor = true,
            sync_root_with_cwd = false,
            diagnostics = {
                enable = true,
            },
            view = {
                adaptive_size = false,
            },
            renderer = {
                highlight_git = false,
                highlight_opened_files = "name",
                highlight_modified = "none",
                indent_markers = {
                    enable = true,
                },
                add_trailing = true,
                group_empty = true,
                full_name = true,
                icons = {
                    git_placement = "after",
                }
            },
            filters = {
                dotfiles = false, -- Disables showing dotfiles by default
            },
            actions = {
                open_file = {
                    quit_on_open = true,
                    window_picker = {
                        chars = "1234567890",
                    },
                },
            },
            update_focused_file = {
                enable = true,
                update_root = false,
            },
            modified = {
                enable = true,
                show_on_dirs = true,
                show_on_open_dirs = false
            }
        }
    },

    {
        "folke/which-key.nvim",
        lazy = true,

        opts = {
            plugins = {
                marks = true,
                registers = true,
                spelling = {
                    enabled = true,
                    suggestions = 20
                }
            },
            window = {
                border = "none",          -- none, single, double, shadow
                position = "bottom",      -- bottom, top
                margin = { 1, 0, 1, 0 },  -- extra window margin [top, right, bottom, left]
                padding = { 1, 0, 1, 0 }, -- extra window padding [top, right, bottom, left]
                winblend = 0
            },
            layout = {
                width = { min = 10, max = 50 },
                height = { min = 5, max = 10 },
                spacing = 3,
            },
            ignore_missing = false
        }
    },

    {
        "lukas-reineke/indent-blankline.nvim", -- Indentation guides
        event = { "BufReadPost", "BufNewFile" },
        main = "ibl",

        opts = {
            exclude = {
                filetypes = { "dashboard" }
            }
        }

    },

    {
        "lewis6991/gitsigns.nvim",
        event = { "BufReadPost", "BufNewFile" },

        opts = {
            signs                        = {
                add          = { hl = 'GitSignsAdd', text = '│', numhl = 'GitSignsAddNr', linehl = 'GitSignsAddLn' },
                change       = { hl = 'GitSignsChange', text = '│', numhl = 'GitSignsChangeNr',
                    linehl = 'GitSignsChangeLn' },
                delete       = { hl = 'GitSignsDelete', text = '_', numhl = 'GitSignsDeleteNr',
                    linehl = 'GitSignsDeleteLn' },
                topdelete    = { hl = 'GitSignsDelete', text = '‾', numhl = 'GitSignsDeleteNr',
                    linehl = 'GitSignsDeleteLn' },
                changedelete = { hl = 'GitSignsChange', text = '~', numhl = 'GitSignsChangeNr',
                    linehl = 'GitSignsChangeLn' },
            },
            signcolumn                   = true, -- Toggle with `:Gitsigns toggle_signs`
            numhl                        = true, -- Toggle with `:Gitsigns toggle_numhl`
            linehl                       = false, -- Toggle with `:Gitsigns toggle_linehl`
            word_diff                    = false, -- Toggle with `:Gitsigns toggle_word_diff`
            watch_gitdir                 = {
                interval = 1000,
                follow_files = true
            },
            attach_to_untracked          = true,
            current_line_blame           = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
            current_line_blame_opts      = {
                virt_text = true,
                virt_text_pos = 'eol', -- 'eol' | 'overlay' | 'right_align'
                delay = 1000,
                ignore_whitespace = false,
            },
            current_line_blame_formatter = '<author>, <author_time:%Y-%m-%d> - <summary>',
            sign_priority                = 6,
            update_debounce              = 100,
            status_formatter             = nil, -- Use default
            max_file_length              = 40000, -- Disable if file is longer than this (in lines)
            preview_config               = {
                -- Options passed to nvim_open_win
                border = 'single',
                style = 'minimal',
                relative = 'cursor',
                row = 0,
                col = 1
            },
            yadm                         = {
                enable = false
            },
        }
    },

    {
        "petertriho/nvim-scrollbar",
        event = { "BufReadPost", "BufNewFile" },
        dependencies = { "lewis6991/gitsigns.nvim" },

        opts = {
            show = true,
            show_in_active_only = false,
            set_highlights = true,
            folds = 1000,                -- handle folds, set to number to disable folds if no. of lines in buffer exceeds this
            max_lines = false,           -- disables if no. of lines in buffer exceeds this
            hide_if_all_visible = false, -- Hides everything if all lines are visible
            throttle_ms = 100,
            handle = {
                text = " ",
                blend = 30,                 -- Integer between 0 and 100. 0 for fully opaque and 100 to full transparent. Defaults to 30.
                color = nil,
                color_nr = nil,             -- cterm
                highlight = "CursorColumn",
                hide_if_all_visible = true, -- Hides handle if all lines are visible
            },
            marks = {
                Cursor = {
                    text = " ",
                    priority = 0,
                    gui = nil,
                    color = nil,
                    cterm = nil,
                    color_nr = nil, -- cterm
                    highlight = "Normal",
                },
                Search = {
                    text = { "-", "=" },
                    priority = 1,
                    gui = nil,
                    color = nil,
                    cterm = nil,
                    color_nr = nil, -- cterm
                    highlight = "Search",
                },
                Error = {
                    text = { "-", "=" },
                    priority = 2,
                    gui = nil,
                    color = nil,
                    cterm = nil,
                    color_nr = nil, -- cterm
                    highlight = "DiagnosticVirtualTextError",
                },
                Warn = {
                    text = { "-", "=" },
                    priority = 3,
                    gui = nil,
                    color = nil,
                    cterm = nil,
                    color_nr = nil, -- cterm
                    highlight = "DiagnosticVirtualTextWarn",
                },
                Info = {
                    text = { "-", "=" },
                    priority = 4,
                    gui = nil,
                    color = nil,
                    cterm = nil,
                    color_nr = nil, -- cterm
                    highlight = "DiagnosticVirtualTextInfo",
                },
                Hint = {
                    text = { "-", "=" },
                    priority = 5,
                    gui = nil,
                    color = nil,
                    cterm = nil,
                    color_nr = nil, -- cterm
                    highlight = "DiagnosticVirtualTextHint",
                },
                Misc = {
                    text = { "-", "=" },
                    priority = 6,
                    gui = nil,
                    color = nil,
                    cterm = nil,
                    color_nr = nil, -- cterm
                    highlight = "Normal",
                },
                GitAdd = {
                    text = "┆",
                    priority = 7,
                    gui = nil,
                    color = nil,
                    cterm = nil,
                    color_nr = nil, -- cterm
                    highlight = "GitSignsAdd",
                },
                GitChange = {
                    text = "┆",
                    priority = 7,
                    gui = nil,
                    color = nil,
                    cterm = nil,
                    color_nr = nil, -- cterm
                    highlight = "GitSignsChange",
                },
                GitDelete = {
                    text = "▁",
                    priority = 7,
                    gui = nil,
                    color = nil,
                    cterm = nil,
                    color_nr = nil, -- cterm
                    highlight = "GitSignsDelete",
                },
            },
            excluded_buftypes = {
                "terminal",
            },
            excluded_filetypes = {
                "cmp_docs",
                "cmp_menu",
                "noice",
                "prompt",
                "TelescopePrompt",
            },
            autocmd = {
                render = {
                    "BufWinEnter",
                    "TabEnter",
                    "TermEnter",
                    "WinEnter",
                    "CmdwinLeave",
                    "TextChanged",
                    "VimResized",
                    "WinScrolled",
                },
                clear = {
                    "BufWinLeave",
                    "TabLeave",
                    "TermLeave",
                    "WinLeave",
                },
            },
            handlers = {
                cursor = true,
                diagnostic = true,
                gitsigns = false, -- Requires gitsigns
                handle = true,
                search = false,   -- Requires hlslens
                ale = false,      -- Requires ALE
            },
        },
    },

    {
        "glepnir/dashboard-nvim",
        event = "VimEnter",

        opts = {
            theme = "doom",
            config = {
                header = {
                    "",
                    "",
                    "",
                    "███▄    █ ▓█████  ▒█████  ▓█████▄  ██▓ ███▄    █   ▄████  █    ██   ██████  ",
                    "██ ▀█   █ ▓█   ▀ ▒██▒  ██▒▒██▀ ██▌▓██▒ ██ ▀█   █  ██▒ ▀█▒ ██  ▓██▒▒██    ▒  ",
                    "▓██  ▀█ ██▒▒███   ▒██░  ██▒░██   █▌▒██▒▓██  ▀█ ██▒▒██░▄▄▄░▓██  ▒██░░ ▓██▄   ",
                    "▓██▒  ▐▌██▒▒▓█  ▄ ▒██   ██░░▓█▄   ▌░██░▓██▒  ▐▌██▒░▓█  ██▓▓▓█  ░██░  ▒   ██▒",
                    "▒██░   ▓██░░▒████▒░ ████▓▒░░▒████▓ ░██░▒██░   ▓██░░▒▓███▀▒▒▒█████▓ ▒██████▒▒",
                    "░ ▒░   ▒ ▒ ░░ ▒░ ░░ ▒░▒░▒░  ▒▒▓  ▒ ░▓  ░ ▒░   ▒ ▒  ░▒   ▒ ░▒▓▒ ▒ ▒ ▒ ▒▓▒ ▒ ░",
                    "░ ░░   ░ ▒░ ░ ░  ░  ░ ▒ ▒░  ░ ▒  ▒  ▒ ░░ ░░   ░ ▒░  ░   ░ ░░▒░ ░ ░ ░ ░▒  ░ ░",
                    "   ░   ░ ░    ░   ░ ░ ░ ▒   ░ ░  ░  ▒ ░   ░   ░ ░ ░ ░   ░  ░░░ ░ ░ ░  ░  ░  ",
                    "         ░    ░  ░    ░ ░     ░     ░           ░       ░    ░           ░  ",
                    "                            ░                                               ",
                },
                center = {
                    {
                        icon = "  ",
                        icon_hl = "",
                        desc = "Open Personal Configuration",
                        desc_hl = "",
                        key = "Space o C c",
                        key_hl = "Number",
                        action = function() require("nvim-tree.api").tree.open { path = home .. "/.config/nvim/" } end
                    },
                    {
                        icon = " ",
                        icon_hl = "",
                        desc = "Edit plugins",
                        desn_hl = "",
                        key = "Space o C p",
                        key_hl = "Number",
                        action = function() vim.cmd(":edit " .. home .. "/.config/nvim/lua/plugins.lua") end
                    },
                    {
                        icon = " ",
                        icon_hl = "",
                        desc = "Edit keybinds",
                        desn_hl = "",
                        key = "Space o C k",
                        key_hl = "Number",
                        action = function() vim.cmd(":edit " .. home .. "/.config/nvim/lua/keybinds.lua") end
                    },
                },
            },
            hide = {
                statusline = false,
                tabline = true,
                winbar = true
            }
        }
    },

    {
        "rcarriga/nvim-notify",
        event = "VeryLazy",

        opts = {
            timeout = 5,
            fps = 60
        },

        config = function (_, _)
            vim.notify = require("notify")
        end
    }
}
