return {
    {
        "VonHeikemen/lsp-zero.nvim",
        branch = "v3.x",
        lazy = true,

        init = function()
            vim.g.lsp_zero_extend_cmp = 0
            vim.g.lsp_zero_extend_lspconfig = 0
        end,

    },

    {
        "neovim/nvim-lspconfig",
        cmd = { "LspInfo", "LspStart" },
        event = { "BufReadPost", "BufNewFile" },
        dependencies = {
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
            "hrsh7th/cmp-nvim-lsp"
        },

        config = function(_, _)
            local lsp_zero = require("lsp-zero")
            local wk = require("which-key")

            local signs = { Error = "󰅚 ", Warn = "󰀪 ", Hint = "󰌶 ", Info = " " }
            for type, icon in pairs(signs) do
                local hl = "DiagnosticSign" .. type
                vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
            end

            vim.diagnostic.config {
                virtual_text = {
                    spacing = 4,
                    source = "if_many",
                },
                signs = true,
                underline = true,
                update_in_insert = true,
                severity_sort = true,
                inlay_hints = { enabled = false },
                capabalities = {},
                autoformat = true,
                format_notify = false,
            }

            lsp_zero.on_attach(function(client, bufnr)
                lsp_zero.default_keymaps({ buffer = bufnr })

                wk.register({
                    ["gl"] = { vim.diagnostic.open_float, "Open diagnostic window" },
                    ["[d"] = { vim.diagnostic.goto_prev, "Goto previous diagnostic" },
                    ["]d"] = { vim.diagnostic.goto_next, "Goto next diagnostic" }
                }, { buffer = bufnr })
            end)
        end
    },

    {
        "williamboman/mason.nvim",
        cmd = { "Mason", "MasonUpdate", "MasonInstall", "MasonUninstall" },
        build = ":MasonUpdate",

        config = true
    },

    {
        "williamboman/mason-lspconfig.nvim",
        lazy = true,

        opts = function()
            local lsp_zero = require("lsp-zero")
            lsp_zero.extend_lspconfig()

            return {
                ensure_installed = { "lua_ls" },
                handlers = {
                    lsp_zero.default_setup,
                    lua_ls =
                        require('lspconfig').lua_ls.setup {
                            settings = {
                                Lua = {
                                    runtime = {
                                        version = "Lua 5.4"
                                    },
                                    diagnostics = {
                                        -- Get the language server to recognize the `vim` global
                                        globals = { 'vim', "awesome", "client" },
                                    },
                                    hint = {
                                        enable = false,
                                    },
                                    workspace = {
                                        -- Make the server aware of Neovim runtime files
                                        library = vim.api.nvim_get_runtime_file("", true),
                                        checkThirdParty = false,
                                        userThirdParty = {}
                                    },
                                    -- Do not send telemetry data containing a randomized but unique identifier
                                    telemetry = {
                                        enable = true,
                                    }
                                }
                            }
                        }
                },
                hls =
                    require("lspconfig").hls.setup {}
            }
        end
    },
}
