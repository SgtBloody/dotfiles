vim.api.nvim_create_autocmd("BufWritePost", {
    pattern = "sxhkdrc",
    group = vim.api.nvim_create_augroup("Restart", { clear = true }),
    callback = function()
        vim.fn.jobstart("pkill --signal USR1 sxhkd", {
            on_exit = function(_, data)
                if data == 0
                then
                    print("Restarted sxhkd: " .. data)
                else
                    print("fail: " .. data)
                end
            end
        })
    end
})
vim.api.nvim_create_autocmd("BufWritePost", {
    pattern = "plugins.lua",
    group = vim.api.nvim_create_augroup("Update", { clear = true }),
    command = ":PackerSync"
})

--highlights yanked text
vim.api.nvim_create_autocmd("TextYankPost", {
    callback = function () vim.highlight.on_yank { higroup = "Search", timeout = 300 } end })
