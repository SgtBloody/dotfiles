#!/usr/bin/lua

---Wrapper for riverctl shell command
---@param args string
---@return _
local function riverctl(args)
    return os.execute("riverctl " .. args)
end

---@param program string
---@param callback? string
---@return string
local function spawn(program, callback)
    ---@type string
    local command = "spawn '" .. program .. "'"

    if callback
    then
        return riverctl(callback .. " " .. command)
    else
        return riverctl(command)
    end
end

---@param options table
local function setOptions(options)
    for option, args in pairs(options) do
        riverctl(option .. " " .. args)
    end
end

---@param bindings table
---@param mapCommand? string | nil
local function setKeybinds(bindings, mapCommand)
    mapCommand = mapCommand or "map"

    for mode, t in pairs(bindings) do
        for modifier, t2 in pairs(t) do
            for key, command in pairs(t2) do
                ---@type string
                local fullPrerequisites = table.concat({ mapCommand, mode, modifier, key }, " ")

                if type(command) == "function"
                then
                    command(fullPrerequisites)
                else
                    riverctl(fullPrerequisites .. " " .. command)
                end
            end
        end
    end
end

---@param programs table
local function autostart(programs)
    for _, program in pairs(programs) do
        spawn(program)
    end
end

local keybindings = {
    normal = {
        ["Super"] = {
            Return = function(c) return spawn("foot", c) end,
            r = function(c) return spawn("bemenu-run -l 10", c) end,

            c = "close", -- Close focused window
            j = "focus-view next",
            k = "focus-view previous",
            h = "send-layout-cmd rivertile 'main-ratio -0.05'",
            l = "send-layout-cmd rivertile 'main-ratio +0.05'",
            t = "toggle-float",
            f = "toggle-fullscreen",

            F11 = "enter-mode passthrough",
        },

        ["Super+Shift"] = {
            Q = "exit",                  -- Exit river
            J = "swap next",             -- Swap the focused window with the next
            K = "swap previous",         -- Swap the focused window with the previous
            L = "focus-output next",     -- Switch to next monitor
            H = "focus-output previous", -- Switch to previous monitor
        },

        ["Super+Control"] = {
            b = function (c) return spawn("firefox", c) end,
        },

        ["Super+Shift+Control"] = {
        },

        ["None"] = {
            XF86AudioRaiseVolume = function (c) return spawn("amixer set Master 2%+ --quiet", c) end,
            XF86AudioLowerVolume = function (c) return spawn( "amixer set Master 2%- --quiet", c) end,
            XF86AudioMute = function (c) return spawn("amixer set Master toggle --quiet", c) end,
            XF86AudioMicMute = function (c) return spawn("amixer set Capture toggle --quiet", c) end,
            XF86MonBrightnessUp = function (c) return spawn("brightnessctl set +10%", c) end,
            XF86MonBrightnessDown = function (c) return spawn("brightnessctl set 10%-", c) end,
        }
    },
    locked = {},
    passthrough = {
        ["Super"] = { F11 = "enter-mode normal" }
    }
}

-- Set keybinds for tags
do
    ---@param modifier string
    ---@param key string
    ---@param command string
    local function setTags(modifier, key, command)
        keybindings.normal[modifier][key] = command
    end
    for i = 1, 10 do
        if i == 10
        then
            setTags("Super", tostring(0), "set-focused-tags " .. (1 << i - 1))
            setTags("Super+Shift", tostring(0), "set-view-tags " .. (1 << i - 1))
            setTags("Super+Control", tostring(0), "toggle-focused-tags " .. (1 << i - 1))
        else
            setTags("Super", tostring(i), "set-focused-tags " .. (1 << i - 1))
            setTags("Super+Shift", tostring(i), "set-view-tags " .. (1 << i - 1))
            setTags("Super+Control", tostring(i), "toggle-focused-tags " .. (1 << i - 1))
        end
    end
end

local mouseBindings = {
    normal = {
        ["Super"] = {
            BTN_LEFT = "move-view",
            BTN_RIGHT = "resize-view"
        },
    }
}

local options = {
    ["default-layout"] = "rivertile",
    ["focus-follows-cursor"] = "normal",
    ["set-repeat"] = "50 300",
    ["keyboard-layout"] = "gb,cz",

    ["declare-mode"] = "passthrough"
}

local theming = {
    ["background-color"] = "0x000000",
    ["border-color-focused"] = "0xb30059",
    ["border-color-unfocused"] = "0x888888",
    ["border-width"] = "2"
}

local programs = {
    "rivertile -view-padding 4 -outer-padding 4 -main-ratio 0.5",
    "xrdb -load $HOME/.Xresources",
    "waybar",
    "dunst",
    "swaybg -i $HOME/Pictures/Wallpapers/j7c35j0652s91.png"
}

-- FIX: Currently only like this cause I am tired
riverctl("rule-add csd -app-id 'bar'")


setOptions(options)
setOptions(theming)
autostart(programs)
setKeybinds(keybindings)
setKeybinds(mouseBindings, "map-pointer")
