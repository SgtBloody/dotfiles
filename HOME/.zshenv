### XDG Base Directory Specification
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

### Location of dotfiles ###
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export STARSHIP_CONFIG="$XDG_CONFIG_HOME/starship/starship.toml"
export ELINKS_CONFDIR="$XDG_CONFIG_HOME/elinks"

export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GOPATH="$XDG_DATA_HOME/go"

export CUDA_CACHE_PATH="$XDG_CACHE_HOME/nv"

### System ###
export QT_QPA_PLATFORMTHEME="qt5ct"

### Editors ###
# export EDITOR="nano"
# export EDITOR="vim"
export EDITOR="nvim"
# export EDITOR="neovide --nofork"
# export EDITOR="emacsclient -c -a emacs"
# export EDITOR="emacs"

### Pagers ###
# export MANPAGER="less"
# export MANPAGER="sh -c 'col -bx | bat -l man -p'"   # Bat
export MANPAGER='nvim +Man!'
#export MANPAGER="moar"

export MOAR="--style doom-one --no-linenumbers"

### Path ###
export PATH="$PATH:$HOME/Scripts"
export PATH="$PATH:$HOME/AppImages"
export PATH="$PATH:$HOME/.cargo/bin"      # Rust env
export PATH="$PATH:$HOME/.emacs.d/bin"    # Doom Emacs env
export PATH="$PATH:$HOME/.local/bin"

export LUA_PATH='/usr/share/lua/5.4/?.lua;/usr/share/lua/5.4/?/init.lua;/usr/lib/lua/5.4/?.lua;/usr/lib/lua/5.4/?/init.lua;./?.lua;./?/init.lua;/home/bloody/.luarocks/share/lua/5.4/?.lua;/home/bloody/.luarocks/share/lua/5.4/?/init.lua'
export LUA_CPATH='/usr/lib/lua/5.4/?.so;/usr/lib/lua/5.4/loadall.so;./?.so;/home/bloody/.luarocks/lib/lua/5.4/?.so'
export PATH="$PATH:$HOME/.luarocks/bin"

[ -f "/usr/bin/bemenu" ] && [ -f "$XDG_CONFIG_HOME/zsh/options/bemenu.sh" ] && source "$XDG_CONFIG_HOME/zsh/options/bemenu.sh"
