#!/usr/bin/bash
### Editors
# export EDITOR="nano"
# export EDITOR="vim"
# export EDITOR="nvim"
export EDITOR="emacsclient -c"
# export EDITOR="emacs"

### Locations of dotfiles
export STARSHIP_CONFIG=$HOME/.config/starship/starship.toml

### Aliases
alias d="date '+%d/%m/%Y %R'"
alias ls="exa -l --icons"

# Dotfiles management
alias config="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"
alias dots="config status"
alias dota="config add"
alias dotc="config commit -m"
alias dotp="config push git@gitlab.com:sgtbloody/dotfiles"

# Directory/File management
alias ..="cd .."
alias cp="cp -v"
alias mv="mv -vi"
alias rm="rm -vi"
alias mkd="mkdir -v"
alias rmd="rmdir -vi"

# Applications
alias sxiv="sxiv -bf"
alias rick="curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash"

# Kittens
alias icat="kitty +kitten icat --align=left"
alias mdcat="mdcat -p"

# Kitty terminal aliases
alias icat="kitty +kitten icat"
# source <(kitty + complete setup bash)


[ -f ~/.fzf.bash ] && source ~/.fzf.bash
# Starship prompt
eval "$(starship init bash)"
